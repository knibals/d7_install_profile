<?php
/**
 * @file
 * In this, we can alter all the configuration forms. Examples includes:
 * 
 * - Choosing language page (hook_install_tasks_alter()),
 * - Database configuration form (hook_form_FORM_ID_alter())
 * - Initializing default theme, background, logo etc (hook_init)
 * - Defining list of tasks specific to the profile (install_tasks())
 * - etc.
 */

